﻿using Features.Config;
using UnityEngine;
using Zenject;

namespace Features.Input
{
	public class InputCameraController : MonoBehaviour {

		[Inject]
		private IInput Input
		{
			set
			{
				_input = value;
				InitEvents();
			}
		}

		private IInput _input;
		
		[Inject]
		public CharacterConfig CharacterConfig { private set; get; }
		
		private void InitEvents()
		{
			_input.OnRotate += OnRotate;
		}

		private void OnDestroy()
		{
			_input.OnRotate -= OnRotate;
		}

		private void OnRotate(Vector3 rotate)
		{
			transform.Rotate(Vector3.left, rotate.y * Time.deltaTime * CharacterConfig.RotationSpeed);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using Features.Config;
using UnityEngine;
using Zenject;

namespace Features.Input
{
	public class InputGameController : MonoBehaviour
	{
		[Inject]
		private IInput Input
		{
			set
			{
				_input = value;
				InitEvents();
			}
		}

		[Inject]
		public CharacterConfig CharacterConfig { private set; get; }

		private IInput _input;

		private void InitEvents()
		{
			_input.OnMoveForward += MoveForward;
			_input.OnMoveLeft += MoveLeft;
			_input.OnMoveBack += MoveBack;
			_input.OnMoveRight += MoveRight;
			_input.OnRotate += OnRotate;
			_input.OnJump += OnJump;
		}

		private void OnDestroy()
		{
			_input.OnMoveForward -= MoveForward;
			_input.OnMoveLeft -= MoveLeft;
			_input.OnMoveBack -= MoveBack;
			_input.OnMoveRight -= MoveRight;
			_input.OnRotate -= OnRotate;
			_input.OnJump -= OnJump;
		}

		private void MoveForward()
		{
			transform.position += transform.forward * Time.deltaTime * CharacterConfig.MovementSpeed;
		}

		private void MoveBack()
		{
			transform.position += transform.forward * -Time.deltaTime * CharacterConfig.MovementSpeed;
		}

		private void MoveLeft()
		{
			transform.position += transform.right * -Time.deltaTime * CharacterConfig.MovementSpeed;
		}

		private void MoveRight()
		{
			transform.position += transform.right * Time.deltaTime * CharacterConfig.MovementSpeed;
		}

		private void OnRotate(Vector3 rotate)
		{
			transform.Rotate(Vector3.up, rotate.x * Time.deltaTime * CharacterConfig.RotationSpeed);
		}

		private void OnJump()
		{
			Debug.Log("Jump");
		}
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using Features.Input.Editor;
using UnityEngine;

namespace Features.Input.Editor
{
	public class CustomTestInput : IInput
	{
		public event Action OnMoveForward;
		public event Action OnMoveRight;
		public event Action OnMoveLeft;
		public event Action OnMoveBack;
		public event Action<Vector3> OnRotate;
		public event Action OnJump;

		public void MoveForward()
		{
			OnMoveForward.Invoke();
		}

		public void MoveRight()
		{
			OnMoveRight.Invoke();
		}

		public void MoveLeft()
		{
			OnMoveLeft.Invoke();
		}

		public void MoveBack()
		{
			OnMoveBack.Invoke();
		}

		public void Rotate()
		{
			OnRotate.Invoke(Vector3.up);
		}

		public void Jump()
		{
			OnJump.Invoke();
		}
	}
}
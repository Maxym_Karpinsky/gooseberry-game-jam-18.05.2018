using System;
using System.Collections;
using Features.Config;
using Zenject;
using NUnit.Framework;
using Moq;
using UnityEngine;
using UnityEngine.TestTools;

namespace Features.Input.Editor
{
    [TestFixture]
    public class InputTest : ZenjectUnitTestFixture
    {
        private CustomTestInput _customTestInput;
        private InputGameController _inputGameController;

        [SetUp]
        public void SetUp()
        {
            CharacterConfig c = ScriptableObject.CreateInstance<CharacterConfig>();
            
            _customTestInput = new CustomTestInput();
            Container.Bind<IInput>().FromInstance(_customTestInput);
            Container.Bind<CharacterConfig>().FromInstance(c);
            GameObject g = new GameObject("g");
            g.transform.position = Vector3.zero;
            g.transform.rotation = Quaternion.Euler(Vector3.zero);
            _inputGameController = Container.InstantiateComponent<InputGameController>(g);
        }

        [Test]
        public void Input_ValidSubscription_MoveForward()
        {
            _customTestInput.MoveForward();
        }

        [Test]
        public void Input_ValidSubscription_MoveBack()
        {
            _customTestInput.MoveBack();
        }

        [Test]
        public void Input_ValidSubscription_MoveLeft()
        {
            _customTestInput.MoveLeft();
        }

        [Test]
        public void Input_ValidSubscription_MoveRight()
        {
            _customTestInput.MoveRight();
        }

        [Test]
        public void Input_ValidSubscription_Rotate()
        {
            _customTestInput.Rotate();
        }
        
        [Test]
        public void Input_ValidSubscription_Jump()
        {
            _customTestInput.Jump();
        }
    }
}
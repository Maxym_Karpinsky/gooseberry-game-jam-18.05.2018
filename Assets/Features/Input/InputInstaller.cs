using UnityEngine;
using Zenject;

namespace Features.Input
{
    public class InputInstaller : MonoInstaller<InputInstaller>
    {
        public override void InstallBindings()
        {
            GameObject g= new GameObject("GameInput");
            GameInput gameInput = g.AddComponent<GameInput>();
            Container.Bind<IInput>().FromInstance(gameInput);
        }
    }
}
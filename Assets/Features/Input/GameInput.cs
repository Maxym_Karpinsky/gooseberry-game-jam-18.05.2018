﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Features.Input
{
	public class GameInput : MonoBehaviour, IInput
	{

		#region IInuput

		public event Action OnMoveForward;
		public event Action OnMoveRight;
		public event Action OnMoveLeft;
		public event Action OnMoveBack;
		public event Action<Vector3> OnRotate;
		public event Action OnJump;

		#endregion

		private void OnEnable()
		{
			Cursor.visible = false;
		}

		private void Update()
		{
			KeyboardInputUpdateForwardBack();
			KeyboardInputUpdateSides();
			MouseUpdate();
			UpdateOtherKeys();
		}

		private void KeyboardInputUpdateForwardBack()
		{
			if (UnityEngine.Input.GetKey(KeyCode.W))
			{
				OnMoveForward.Invoke();
				return;
			}

			if (UnityEngine.Input.GetKey(KeyCode.S))
			{
				OnMoveBack.Invoke();
			}
		}

		private void KeyboardInputUpdateSides()
		{
			if (UnityEngine.Input.GetKey(KeyCode.A))
			{
				OnMoveLeft.Invoke();
				return;
			}

			if (UnityEngine.Input.GetKey(KeyCode.D))
			{
				OnMoveRight.Invoke();
			}
		}

		private void UpdateOtherKeys()
		{
			if (UnityEngine.Input.GetKeyUp(KeyCode.Space))
			{
				OnJump.Invoke();
			}
		}

		private void MouseUpdate()
		{
			var d1 = UnityEngine.Input.GetAxis("Mouse X");
			var d2 = UnityEngine.Input.GetAxis("Mouse Y");
			
			Vector3 v = new Vector3(d1, d2, 0f);
			OnRotate.Invoke(v);
		}
	}


	public interface IInput
	{
		event Action OnMoveForward;
		event Action OnMoveRight;
		event Action OnMoveLeft;
		event Action OnMoveBack;
		event Action<Vector3> OnRotate;
		event Action OnJump;
	}
}


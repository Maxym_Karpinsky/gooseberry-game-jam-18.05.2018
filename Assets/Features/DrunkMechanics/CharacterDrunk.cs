﻿using Features.Effects;
using UnityEngine;
using Zenject;

namespace Features.DrunkMechanics
{
	public class CharacterDrunk : MonoBehaviour
	{
		[Inject] private DrunkCamera _drunkCamera;

		private void OnTriggerEnter(Collider other)
		{
			if (other.transform.CompareTag("AntiDrunkOject"))
			{
				_drunkCamera.DrunkLevelDown();
				Destroy(other.gameObject);
			}
		}
	}
}

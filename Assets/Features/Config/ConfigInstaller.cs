using Zenject;

namespace Features.Config
{
    public class ConfigInstaller : MonoInstaller<ConfigInstaller>
    {
        public CharacterConfig CharacterConfig;
        
        public override void InstallBindings()
        {
            Container.Bind<CharacterConfig>().FromInstance(CharacterConfig);
        }
    }
}
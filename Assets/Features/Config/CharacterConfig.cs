﻿using UnityEngine;

namespace Features.Config
{
	[CreateAssetMenu(fileName = "CharacterConfig", menuName = "Config/CharacterConfig", order = 1)]
	public class CharacterConfig : ScriptableObject
	{
		[Header("Movement")]
		public float RotationSpeed = 10f;
		public float MovementSpeed = 20f;

		[Header("Drunk Level")]
		public float DecreaseDunkness = 0.1f;
		public float SpeedOfIncreasing = 0.01f;

	}
}

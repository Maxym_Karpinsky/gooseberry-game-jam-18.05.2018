﻿using System;
using Features.Config;
using UnityEngine;
using Zenject;

namespace Features.Effects
{
	[RequireComponent(typeof(Camera))]
	public class DrunkCamera : MonoBehaviour
	{
		[SerializeField] private Material _material;
		[Inject] private CharacterConfig _config;
		
		[Range(0.001f, 1f)]
		[SerializeField] private float _drunkPower = 0.001f;

		private void Start()
		{
			_material.SetFloat("_powerOfDrunk", 0.01f);
		}

		private void Update()
		{
			_drunkPower += Time.deltaTime * _config.SpeedOfIncreasing;
			if (_drunkPower < 1f && _drunkPower > 0f)
			{
				_material.SetFloat("_powerOfDrunk", _drunkPower);
			}
		}

		public void DrunkLevelDown()
		{
			_drunkPower -= _config.DecreaseDunkness;
			_drunkPower = Mathf.Clamp(_drunkPower, 0, _drunkPower);
		}

		private void OnRenderImage(RenderTexture src, RenderTexture dest)
		{
			Graphics.Blit(src, dest, _material);
		}
	}
}

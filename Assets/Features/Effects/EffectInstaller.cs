using UnityEngine;
using Zenject;

namespace Features.Effects
{
    public class EffectInstaller : MonoInstaller<EffectInstaller>
    {
        [SerializeField] private DrunkCamera _drunkCamera;
        
        public override void InstallBindings()
        {
            Container.Bind<DrunkCamera>().FromInstance(_drunkCamera);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

namespace GameJam
{
	public class FieldGenerator : MonoBehaviour
	{
		[SerializeField] private Grid2D _grid2D;

		[SerializeField] private Building _buildingPrefab;

		public void Generate()
		{
			for (int i = this.transform.childCount - 1; i >= 0; --i)
			{
				DestroyImmediate(this.transform.GetChild(i).gameObject);
			}

			for (int x = 0; x < this._grid2D.Nodes_.GetLength(0); x += 3)
			{
				for (int z = 0; z < this._grid2D.Nodes_.GetLength(1); z += 3)
				{
					Instantiate(this._buildingPrefab, this._grid2D.Nodes_[x, z].WorldPosition_ + new Vector3(this._grid2D._NodeSize / 2f, this._buildingPrefab.transform.position.y, this._grid2D._NodeSize / 2f), Quaternion.identity, this.transform);
				}
			}
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{
		//}
#endif
	}
}

namespace GameJam.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(FieldGenerator))]
	[CanEditMultipleObjects]
	public class FieldGeneratorEditor : Editor
	{
#pragma warning disable 0219
		private FieldGenerator _sFieldGenerator;
#pragma warning restore 0219

		private void OnEnable()
		{
			this._sFieldGenerator = target as FieldGenerator;
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			if (GUILayout.Button("Generate"))
			{
				this._sFieldGenerator.Generate();
			}
		}
	}
#endif
}
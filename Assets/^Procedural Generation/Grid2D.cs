﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

namespace GameJam
{
	public class Grid2D : MonoBehaviour
	{
		[SerializeField] private int _xNodesQuantity = 10;
		public int _XNodesQuantity { get { return this._xNodesQuantity; } }

		[SerializeField] private int _zNodesQuantity = 10;
		public int _ZNodesQuantity { get { return this._zNodesQuantity; } }

		[SerializeField] private float _nodeSize = 1f;
		public float _NodeSize { get { return this._nodeSize; } }

		[SerializeField] public Node[,] Nodes_ { get; private set; }

		public void GenerateGrid()
		{
			this.Nodes_ = new Node[this._xNodesQuantity, this._zNodesQuantity];

			for (int x = 0; x < this._xNodesQuantity; x++)
			{
				for (int z = 0; z < this._zNodesQuantity; z++)
				{
					this.Nodes_[x, z] = new Node(new Vector3(this._nodeSize * x + this._nodeSize / 2f, 0f, this._nodeSize * z + this._nodeSize / 2f));
				}
			}
		}

		private void OnValidate()
		{
			this.GenerateGrid();
		}

#if UNITY_EDITOR
		protected virtual void OnDrawGizmos()
		{
			if (this.Nodes_ == null)
			{
				this.GenerateGrid();
			}

			Gizmos.color = Color.black;
			for (int x = 0; x < this.Nodes_.GetLength(0); x++)
			{
				for (int z = 0; z < this.Nodes_.GetLength(1); z++)
				{
					Gizmos.DrawWireCube(this.Nodes_[x, z].WorldPosition_, new Vector3(this._nodeSize - 0.1f, 0f, this._nodeSize - 0.1f));
				}
			}

			Gizmos.color = Color.white;
			Gizmos.DrawWireCube(this.transform.position + new Vector3(this._xNodesQuantity * this._nodeSize / 2f, 0f, this._zNodesQuantity * this._nodeSize / 2f), new Vector3(this._xNodesQuantity * this._nodeSize, 0f, this._zNodesQuantity * this._nodeSize));
		}
#endif
	}
}

namespace GameJam.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(Grid2D))]
	[CanEditMultipleObjects]
	public class Grid2DEditor : Editor
	{
#pragma warning disable 0219
		private Grid2D _sGrid2D;
#pragma warning restore 0219

		private void OnEnable()
		{
			this._sGrid2D = target as Grid2D;
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
		}
	}
#endif
}
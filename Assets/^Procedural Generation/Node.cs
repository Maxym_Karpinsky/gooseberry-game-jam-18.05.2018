﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

namespace GameJam
{
	public class Node
	{
		public Vector3 WorldPosition_ { get; private set; }

		public Node()
		{

		}

		public Node(Vector3 worldPosition_)
		{
			this.WorldPosition_ = worldPosition_;
		}
	}
}
﻿/* Created by Max.K.Kimo */

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

namespace GameJam
{
	public class Building : MonoBehaviour
	{
		[SerializeField] private BlockedBuilding _substituteBuildingPrefab;

		public void Substitute()
		{
			this.gameObject.SetActive(false);

			Instantiate(this._substituteBuildingPrefab, this.transform.position, Quaternion.identity, this.transform.parent);
		}

		private void OnCollisionEnter(Collision collision)
		{
			this.Substitute();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{
		//}
#endif
	}
}

namespace GameJam.UTILITY
{
#if UNITY_EDITOR
	[CustomEditor(typeof(Building))]
	[CanEditMultipleObjects]
	public class BuildingEditor : Editor
	{
#pragma warning disable 0219
		private Building _sBuilding;
#pragma warning restore 0219

		private void OnEnable()
		{
			this._sBuilding = target as Building;
		}

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
		}
	}
#endif
}
﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

using TMPro;

namespace GameJam
{
    public class BlockedBuilding : MonoBehaviour
    {
		private Rigidbody[] _childrenRigidbodies;

		private void OnTriggerEnter(Collider other)
		{
			for (int i = 0; i < this._childrenRigidbodies.Length; i++)
			{
				this._childrenRigidbodies[i].isKinematic = false;
				this._childrenRigidbodies[i].detectCollisions = true;
			}
		}

		private void OnTriggerExit(Collider other)
		{
			for (int i = 0; i < this._childrenRigidbodies.Length; i++)
			{
				this._childrenRigidbodies[i].isKinematic = true;
				this._childrenRigidbodies[i].detectCollisions = false;
			}
		}

		private void Awake()
		{
			this._childrenRigidbodies = this.GetComponentsInChildren<Rigidbody>();
		}

#if UNITY_EDITOR
		//protected override void OnDrawGizmos()
		//{
		//}
#endif
	}
}

namespace GameJam.UTILITY
{
#if UNITY_EDITOR
    [CustomEditor(typeof(BlockedBuilding))]
    [CanEditMultipleObjects]
    public class BlockedBuildingEditor : Editor
    {
#pragma warning disable 0219
        private BlockedBuilding _sBlockedBuilding;
#pragma warning restore 0219

        private void OnEnable()
        {
            this._sBlockedBuilding = target as BlockedBuilding;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
#endif
}